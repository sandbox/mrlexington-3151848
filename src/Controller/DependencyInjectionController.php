<?php

namespace Drupal\dependency_injection_examples\Controller;

use Drupal\Core\Controller\ControllerBase;

use Symfony\Component\DependencyInjection\ContainerInterface;

use Drupal\Core\Session\AccountProxyInterface;
use Drupal\dependency_injection_examples\CustomService;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\Query\QueryFactory;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\file\FileStorageInterface;
use Drupal\Core\Form\FormBuilder;
use Drupal\image\ImageStyleStorageInterface;
use Drupal\node\NodeStorageInterface;
use Drupal\Core\Password\PasswordInterface;
use Drupal\user\UserStorageInterface;

class DependencyInjectionController extends ControllerBase {
  
  /**
   * @var Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;
  
  /**
   * @var Drupal\dependency_injection_examples\CustomService
   */
  protected $customService;
  
  /**
   * @var Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;
  
  /*
   * @var Drupal\Core\Entity\Query\QueryFactory
   */
  protected $entityQuery;
  
  /**
   * @var Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;
  
  /**
   * @var Drupal\file\FileStorageInterface
   */
  protected $fileStorage;
  
  /**
   * @var Drupal\Core\Form\FormBuilder
   */
  protected $formBuilder;
  
  /**
   * @var Drupal\image\ImageStyleStorageInterface
   */
  protected $imageStyleStorage;
  
  /**
   * @var Drupal\node\NodeStorageInterface
   */
  protected $nodeStorage;
  
  /**
   * @var Drupal\Core\Password\PasswordInterface;
   */
  protected $passwordHasher;
    
  /**
   * @var Drupal\user\UserStorageInterface
   */
  protected $userStorage;

  /**
   * @param Drupal\Core\Session\AccountProxyInterface $currentUser
   * @param Drupal\dependency_injection_examples\CustomService $customService
   * @param Drupal\Core\Datetime\DateFormatterInterface $dateFormatter
   * @param Drupal\Core\Entity\Query\QueryFactory $entityQuery
   * @param Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   * @param Drupal\Core\Form\FormBuilder $formBuilder
   * @param Drupal\file\FileStorageInterface $fileStorage
   * @param Drupal\image\ImageStyleStorageInterface $imageStyleStorage
   * @param Drupal\node\NodeStorageInterface $nodeStorage
   * @param Drupal\user\UserStorageInterface $userStorage
   */
  function __construct(
      AccountProxyInterface $currentUser,
      CustomService $customService,
      DateFormatterInterface $dateFormatter,
      QueryFactory $entityQuery,
      EntityTypeManagerInterface $entityTypeManager,
      FormBuilder $formBuilder,
      FileStorageInterface $fileStorage,
      ImageStyleStorageInterface $imageStyleStorage,
      NodeStorageInterface $nodeStorage,
      PasswordInterface $password_hasher,
      UserStorageInterface $userStorage
    ) {
    
    $this->currentUser = $currentUser;
    $this->customService = $customService;
    $this->dateFormatter = $dateFormatter;
    $this->entityQuery = $entityQuery;
    $this->entityTypeManager = $entityTypeManager;
    $this->fileStorage = $fileStorage;
    $this->formBuilder = $formBuilder;
    $this->imageStyleStorage = $imageStyleStorage;
    $this->nodeStorage = $nodeStorage;
    $this->password_hasher = $passwordHasher;
    $this->userStorage = $userStorage;
  }
  
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('current_user'),
      $container->get('dependency_injection_examples.custom_service'),
      $container->get('date.formatter'),
      $container->get('entity.query'),
      $container->get('entity_type.manager'),
      $container->get('entity_type.manager')->getStorage('file'),
      $container->get('form_builder'),
      $container->get('entity_type.manager')->getStorage('image_style'),
      $container->get('entity_type.manager')->getStorage('node'),
      $container->get('password'),
      $container->get('entity_type.manager')->getStorage('user')
    );
  }
  
  public function controllerFunction() {
    
    //Example uses:
    //$user = $this->currentUser->id();
    //$variable = $this->customService->customServiceFunction();
    //$date = $this->dateFormatter->formatInterval(REQUEST_TIME - $node->created->value);    
    //$query = $this->entityQuery->get('node');    
      //$query->condition('type', 'article');
      //$node_ids = $query->execute();    
    //$node = $this->entityTypeManager->getStorage('node');
    //$file = $this->fileStorage->load(123);
    //$form = $this->formBuilder->getForm('\Drupal\module_name\Form\FormClassName');
    //$style = $this->imageStyleStorage->load('image_style_machine_name')->buildUrl('file/url');
    //$node = $this->nodeStorage->load(123);
    //$pass = $this->password_hasher->check($user_pass, $hashedPassword);
    //$user = $this->userStorage->load(123);

    return [
      '#markup' => $this->t('Dependency Injection Examples'),
    ];
  }
  
}
