<?php

namespace Drupal\dependency_injection_examples;

use Drupal\Core\Session\AccountProxyInterface;
use Drupal\dependency_injection_examples\CustomService;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\Query\QueryFactory;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\file\FileStorageInterface;
use Drupal\Core\Form\FormBuilder;
use Drupal\image\ImageStyleStorageInterface;
use Drupal\node\NodeStorageInterface;
use Drupal\Core\Password\PasswordInterface;
use Drupal\user\UserStorageInterface;

class InjectedService {

  /**
   * @var Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;
  
  /**
   * @var Drupal\dependency_injection_examples\CustomService
   */
  protected $customService;
  
  /**
   * @var Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;
  
  /*
   * @var Drupal\Core\Entity\Query\QueryFactory
   */
  protected $entityQuery;
  
  /**
   * @var Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;
  
  /**
   * @var Drupal\file\FileStorageInterface
   */
  protected $fileStorage;
  
  /**
   * @var Drupal\Core\Form\FormBuilder
   */
  protected $formBuilder;
  
  /**
   * @var Drupal\image\ImageStyleStorageInterface
   */
  protected $imageStyleStorage;
  
  /**
   * @var Drupal\node\NodeStorageInterface
   */
  protected $nodeStorage;
  
  /**
   * @var Drupal\Core\Password\PasswordInterface;
   */
  protected $passwordHasher;
    
  /**
   * @var Drupal\user\UserStorageInterface
   */
  protected $userStorage;

  /**
   * @param Drupal\Core\Session\AccountProxyInterface $currentUser
   * @param Drupal\dependency_injection_examples\CustomService $customService
   * @param Drupal\Core\Datetime\DateFormatterInterface $dateFormatter
   * @param Drupal\Core\Entity\Query\QueryFactory $entityQuery
   * @param Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   * @param Drupal\Core\Form\FormBuilder $formBuilder
   * @param Drupal\file\FileStorageInterface $fileStorage
   * @param Drupal\image\ImageStyleStorageInterface $imageStyleStorage
   * @param Drupal\node\NodeStorageInterface $nodeStorage
   * @param Drupal\user\UserStorageInterface $userStorage
   */
  function __construct(
      AccountProxyInterface $currentUser,
      CustomService $customService,
      DateFormatterInterface $dateFormatter,
      QueryFactory $entityQuery,
      EntityTypeManagerInterface $entityTypeManager,
      FormBuilder $formBuilder,
      FileStorageInterface $fileStorage,
      ImageStyleStorageInterface $imageStyleStorage,
      NodeStorageInterface $nodeStorage,
      PasswordInterface $password_hasher,
      UserStorageInterface $userStorage
    ) {
    
    $this->currentUser = $currentUser;
    $this->customService = $customService;
    $this->dateFormatter = $dateFormatter;
    $this->entityQuery = $entityQuery;
    $this->entityTypeManager = $entityTypeManager;
    $this->fileStorage = $fileStorage;
    $this->formBuilder = $formBuilder;
    $this->imageStyleStorage = $imageStyleStorage;
    $this->nodeStorage = $nodeStorage;
    $this->password_hasher = $passwordHasher;
    $this->userStorage = $userStorage;
  }
  
  //'create' function is not required for a service.
  
  public function injectedServiceFunction() {
        
    return 42;
  }
  
}

