<?php

namespace Drupal\dependency_injection_examples\Plugin\Block;

use Drupal\Core\Block\BlockBase;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\user\UserStorageInterface;

/**
 * @Block(
 *   id = "dependency_injection_block",
 *   admin_label = @Translation("Dependency Injection Block"),
 *   category = @Translation("Dependency Injection Examples")
 * )
 */
class DependencyInjectionBlock extends BlockBase implements ContainerFactoryPluginInterface {
  
  /**
   * @var Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;
  
  /**
   * @var Drupal\user\UserStorageInterface
   */
  protected $userStorage;
    
  /**
   * @param Drupal\Core\Session\AccountProxyInterface $currentUser
   * @param Drupal\user\UserStorageInterface $userStorage
   */
  function __construct(array $configuration, $plugin_id, $plugin_definition, AccountProxyInterface $currentUser, UserStorageInterface $userStorage) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    
    $this->currentUser = $currentUser;
    $this->userStorage = $userStorage;
  }
  
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('current_user'),
      $container->get('entity_type.manager')->getStorage('user')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    

    
    return [

    ];
  }

}
